﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CuentaPorCobrar.Startup))]
namespace CuentaPorCobrar
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
