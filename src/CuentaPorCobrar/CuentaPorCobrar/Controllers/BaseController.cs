﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CuentaPorCobrar.DataAccess.Contexto;
using System.Web.Mvc;
using CuentaPorCobrar.DataAccess.Modelos;

namespace CuentaPorCobrar.Controllers
{
    public abstract class BaseController : Controller
    {
        protected readonly CXCContexto Contexto = new CXCContexto();

        protected List<SelectListItem> ObtenerLista<T>(Func<T, SelectListItem> selector, SelectListItem porDefecto = null, bool valorPorDefecto = false) where T : class
        {
            List<T> tipoDocumentos = Contexto.Set<T>().ToList();
            List<SelectListItem> listaDesplegable = new List<SelectListItem>();

            if (valorPorDefecto && porDefecto != null)
            {
                listaDesplegable.Add(porDefecto);
            }

            listaDesplegable = listaDesplegable.Concat(tipoDocumentos.ToList().Select(selector)).ToList();

            return listaDesplegable;
        }

        /// <summary>
        /// Metodo para Llenar la lista del menu desplegable de Tipo de Documentos
        /// </summary>
        /// <param name="valorPorDefecto">Si es verdadero el menu tendra una opcion nula</param>
        /// <returns></returns>
        protected List<SelectListItem> ObtenerTipoDocumento(bool valorPorDefecto = false)
        {
            return ObtenerLista<TipoDocumento>(c => new SelectListItem() { Text = c.Descripcion, Value = c.TipoDocumentoId.ToString() }, new SelectListItem()
            {
                Text = "Sin Tipo De Documento",
                Value = "",
                Selected = true
            }, valorPorDefecto);
        }

        /// <summary>
        /// Metodo para Llenar la lista del menu desplegable de Clientes
        /// </summary>
        /// <param name="valorPorDefecto">Si es verdadero el menu tendra una opcion nula</param>
        /// <returns></returns>
        protected List<SelectListItem> ObtenterClientes(bool valorPorDefecto = false)
        {
            return ObtenerLista<Cliente>(c => new SelectListItem() { Text = c.Nombre + "-" + c.Cedula, Value = c.ClienteId.ToString() }, new SelectListItem()
                {
                    Text = "Sin Cliente especifico",
                    Value = "",
                    Selected = true
                }, valorPorDefecto);
        }

        /// <summary>
        /// Metodo para Llenar la lista del menu desplegable de Cuentas
        /// </summary>
        /// <param name="valorPorDefecto">Si es verdadero el menu tendra una opcion nula</param>
        /// <returns></returns>
        protected List<SelectListItem> ObtenerCuentas(bool valorPorDefecto = false)
        {
            return ObtenerLista<Cuenta>(c => new SelectListItem() { Text = c.Nombre, Value = c.CuentaId.ToString() }, new SelectListItem()
                {
                    Text = "Sin Cuenta",
                    Value = "",
                    Selected = true
                }, valorPorDefecto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Contexto.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}