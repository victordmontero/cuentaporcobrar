﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CuentaPorCobrar.DataAccess.Modelos;
using CuentaPorCobrar.DataAccess.Contexto;
using CuentaPorCobrar.ViewModels;
using AutoMapper;

namespace CuentaPorCobrar.Controllers
{
    [Authorize]
    public class TipoDocumentoController : BaseController
    {
        // GET: /TipoDocumento/
        public ActionResult Index(string criterio)
        {
            List<TipoDocumentoViewModel> tipoDocVms = new List<TipoDocumentoViewModel>();
            var tipoDocumentos = Contexto.TiposDocumento.Where(c =>
                (string.IsNullOrEmpty(criterio) ||
                c.Descripcion.ToLower().Contains(criterio.ToLower())
                ) && c.Estado).ToList();

            foreach (TipoDocumento tipoDocumento in tipoDocumentos)
            {
                var tipoDocVm = new TipoDocumentoViewModel();
                Mapper.DynamicMap(tipoDocumento, tipoDocVm);
                tipoDocVms.Add(tipoDocVm);
            }

            return View(tipoDocVms);
        }

        // GET: /TipoDocumento/Create
        public ActionResult Crear()
        {
            ViewBag.ListaCuentas = ObtenerCuentas();
            return View();
        }

        // POST: /TipoDocumento/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Crear(TipoDocumentoViewModel tipodocumento)
        {
            try
            {
                TipoDocumento tipoDocumentoNuevo = new TipoDocumento();
                Mapper.DynamicMap(tipodocumento, tipoDocumentoNuevo);
                Contexto.TiposDocumento.Add(tipoDocumentoNuevo);
                Contexto.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }

        }

        // GET: /TipoDocumento/Edit/5
        public ActionResult Editar(int? id)
        {
            TipoDocumentoViewModel tipoDocVm = new TipoDocumentoViewModel();
            var tipoDocumento = Contexto.TiposDocumento
                .SingleOrDefault(c => c.TipoDocumentoId == id);
            ViewBag.ListaCuentas = ObtenerCuentas();
            Mapper.DynamicMap(tipoDocumento, tipoDocVm);
            return View(tipoDocVm);
        }

        // POST: /TipoDocumento/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(TipoDocumentoViewModel tipodocumento)
        {
            try
            {
                TipoDocumento tipoDocumentoNuevo = new TipoDocumento();
                Mapper.DynamicMap(tipodocumento, tipoDocumentoNuevo);
                Contexto.Entry(tipoDocumentoNuevo).State = EntityState.Modified;
                Contexto.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                return View();
            }
        }

        // GET: /TipoDocumento/Delete/5
        public ActionResult Eliminar(int id)
        {
            TipoDocumento tipodocumento = Contexto.TiposDocumento.Find(id);
            tipodocumento.Estado = false;
            Contexto.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
