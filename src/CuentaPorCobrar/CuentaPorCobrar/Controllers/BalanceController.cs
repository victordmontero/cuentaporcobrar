﻿using AutoMapper;
using CuentaPorCobrar.DataAccess.Modelos;
using CuentaPorCobrar.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CuentaPorCobrar.Controllers
{
    [Authorize]
    public class BalanceController : BaseController
    {
        //
        // GET: /Balance/
        public async Task<ActionResult> Index(string criterio)
        {
            return await Task.Run(() =>
            {
                List<BalanceViewModel> vms = new List<BalanceViewModel>();
                var balances = Contexto.Balances.ToList().Where(c =>
                    (string.IsNullOrEmpty(criterio) || c.Cliente.Nombre.Contains(criterio)) ||
                    (string.IsNullOrEmpty(criterio) || c.Corte.ToString("dd-MM-yyyy").Contains(criterio)) ||
                    (string.IsNullOrEmpty(criterio) || c.Monto.ToString().Contains(criterio))).ToList();

                foreach (var balance in balances)
                {
                    var vm = new BalanceViewModel();
                    Mapper.DynamicMap(balance, vm);
                    vms.Add(vm);
                }

                return View(vms);
            });
        }
    }
}
