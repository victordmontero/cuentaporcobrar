﻿using AutoMapper;
using CuentaPorCobrar.DataAccess.Modelos;
using CuentaPorCobrar.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CuentaPorCobrar.Controllers
{
    [Authorize]
    public class CuentaController : BaseController
    {
        // GET: Cuenta
        public ActionResult Index()
        {
            return View(Contexto.Cuentas.ToList());
        }

        // GET: Cuenta/Create
        public ActionResult Crear()
        {
            return View();
        }

        // POST: Cuenta/Create
        [HttpPost]
        public ActionResult Crear(CuentaViewModel cuenta)
        {
            try
            {
                Cuenta cuentaNueva = new Cuenta();
                Mapper.DynamicMap(cuenta, cuentaNueva);
                Contexto.Cuentas.Add(cuentaNueva);
                Contexto.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Cuenta/Edit/5
        public ActionResult Editar(int id)
        {
            return View(Contexto.Cuentas.Find(id));
        }

        // POST: Cuenta/Edit/5
        [HttpPost]
        public ActionResult Editar(CuentaViewModel cuenta)
        {
            try
            {
                Cuenta cuentaModificada = new Cuenta();
                Mapper.DynamicMap(cuenta, cuentaModificada);
                Contexto.Entry(cuentaModificada).State = EntityState.Modified;
                Contexto.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Cuenta/Delete/5
        public ActionResult Eliminar(int id)
        {
            try
            {
                var cuenta = Contexto.Cuentas.Find(id);
                Contexto.Cuentas.Remove(cuenta);
                Contexto.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
