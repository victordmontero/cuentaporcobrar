﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CuentaPorCobrar.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class ReporteController : BaseController
    {
        // GET: Reporte
        public ActionResult Generar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Generar(int antiguedadSaldo = 0)
        {
            Session["antiguedadSaldo"] = antiguedadSaldo;
            return Redirect("~/Reporte/Reporte.aspx");
        }
    }
}