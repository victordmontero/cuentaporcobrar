﻿using AutoMapper;
using CuentaPorCobrar.DataAccess.Modelos;
using CuentaPorCobrar.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CuentaPorCobrar.Controllers
{
    [Authorize]
    public class TransaccionController : BaseController
    {
        //
        // GET: /Transaccion/
        public async Task<ActionResult> Index(string criterio)
        {
            return await Task.Run(() =>
            {
                List<TransaccionViewModel> transaccionVms = new List<TransaccionViewModel>();
                var resultado = Contexto.Transacciones.Where(c =>
                    (string.IsNullOrEmpty(criterio) || c.Cliente.Nombre.Contains(criterio)) ||
                    (string.IsNullOrEmpty(criterio) || c.TipoDocumento.Descripcion.Contains(criterio))
                ).ToList();

                foreach (Transaccion transaccion in resultado)
                {
                    var transaccionVm = new TransaccionViewModel();
                    Mapper.DynamicMap(transaccion, transaccionVm);
                    transaccionVms.Add(transaccionVm);
                }

                return View(transaccionVms);
            });
        }

        //
        // GET: /Transaccion/Create
        public ActionResult Crear()
        {
            ViewBag.TiposDocumento = ObtenerTipoDocumento();
            ViewBag.Clientes = ObtenterClientes();
            return View();
        }

        //
        // POST: /Transaccion/Create
        [HttpPost]
        public ActionResult Crear(TransaccionViewModel transaccion)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Transaccion transaccionNueva = new Transaccion();
                    //AfectarBalance(transaccion.ClienteId, transaccion.Monto, transaccion.TipoMovimiento);
                    Mapper.DynamicMap(transaccion, transaccionNueva);
                    Contexto.Transacciones.Add(transaccionNueva);
                    Contexto.SaveChanges();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Crear");
            }
            catch
            {
                return RedirectToAction("Crear");
            }
        }

        private void AfectarBalance(int clienteId, decimal transaccionMonto, TipoMovimiento tipoMovimiento)
        {
            var balance = Contexto.Balances.Single(c => c.ClienteId == clienteId);

            switch (tipoMovimiento)
            {
                case TipoMovimiento.Credito:
                    balance.Monto -= transaccionMonto;
                    break;
                case TipoMovimiento.Debito:
                    balance.Monto += transaccionMonto;
                    break;
                default:
                    break;
            }

            Contexto.SaveChanges();
        }

    }
}
