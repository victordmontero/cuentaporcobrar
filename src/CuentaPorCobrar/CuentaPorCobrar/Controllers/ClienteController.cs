﻿using AutoMapper;
using CuentaPorCobrar.DataAccess.Modelos;
using CuentaPorCobrar.Util;
using CuentaPorCobrar.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CuentaPorCobrar.Controllers
{
    [Authorize]
    public class ClienteController : BaseController
    {
        //
        // GET: /Cliente/
        public async Task<ActionResult> Index(string criterio)
        {
            return await Task.Run(() =>
            {
                var clienteVMS = new List<ClienteViewModel>();
                var clientes = Contexto.Clientes.Where(c =>
                    (string.IsNullOrEmpty(criterio) ||
                    (c.Nombre + c.Cedula).ToLower().Contains(criterio.ToLower()))).ToList();

                foreach (var elemento in clientes)
                {
                    var vm = new ClienteViewModel();
                    Mapper.DynamicMap(elemento, vm);
                    clienteVMS.Add(vm);
                }

                return View(clienteVMS);
            });
        }

        //
        // GET: /Cliente/Create
        public ActionResult Crear()
        {
            return View();
        }

        //
        // POST: /Cliente/Create
        [HttpPost]
        public ActionResult Crear(ClienteViewModel cliente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!Helper.ValidarCedula(cliente.Cedula))
                    {
                        ModelState.AddModelError("Cedula", "Cedula no válida");
                        return View(cliente);
                    }
                    Cliente nuevoCliente = new Cliente();

                    cliente.Cedula = cliente.Cedula.Replace("-", "").Trim();
                    Mapper.DynamicMap(cliente, nuevoCliente);

                    CrearBalancePara(nuevoCliente);

                    Contexto.Clientes.Add(nuevoCliente);

                    Contexto.SaveChanges();

                    return RedirectToAction("Index");
                }
                return View();
            }
            catch (DbUpdateException dbuex)
            {
                ModelState.AddModelError("Cedula", "Ya existe un cliente con esta cédula");
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        //
        // GET: /Cliente/Edit/5
        public ActionResult Editar(int id)
        {
            var clienteExistente = Contexto.Clientes
                    .SingleOrDefault(c => c.ClienteId == id);
            var vm = new ClienteViewModel();
            Mapper.DynamicMap(clienteExistente, vm);
            return View(vm);
        }

        //
        // POST: /Cliente/Edit/5
        [HttpPost]
        public ActionResult Editar(ClienteViewModel cliente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!Helper.ValidarCedula(cliente.Cedula))
                    {
                        ModelState.AddModelError("Cedula", "Cedula no válida");
                        return View(cliente);
                    }
                }
                Cliente clienteModificado = new Cliente();
                Mapper.DynamicMap(cliente, clienteModificado);
                Contexto.Entry(clienteModificado).State = EntityState.Modified;
                Contexto.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Cliente/Delete/5
        public ActionResult Eliminar(int id)
        {
            try
            {
                var clientes = Contexto.Clientes
                    .SingleOrDefault(c => c.ClienteId == id);
                Contexto.Clientes.Remove(clientes);
                Contexto.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private void CrearBalancePara(Cliente nuevoCliente)
        {
            Contexto.Balances.Add(new Balance()
            {
                ClienteId = nuevoCliente.ClienteId,
                Corte = DateTime.Today,
                Monto = 0
            });
        }
    }
}
