﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CuentaPorCobrar.Util
{
    public static class Helper
    {
        public static bool ValidarCedula(string cedula)
        {
            int vnTotal = 0;
            if (string.IsNullOrEmpty(cedula)) return false;
            string vcCedula = cedula.Trim().Replace("-", "");
            int pLongCed = vcCedula.Length;
            int[] digitoMult = new int[11] { 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1 };

            if (pLongCed != 11)
                return false;

            for (int vDig = 1; vDig <= pLongCed; vDig++)
            {
                //Multiplica cada uno de los digitos por uno o dos.
                int vCalculo = Int32.Parse(vcCedula.Substring(vDig - 1, 1)) * digitoMult[vDig - 1];
                if (vCalculo < 10)
                    vnTotal += vCalculo;
                else
                    vnTotal += Int32.Parse(vCalculo.ToString().Substring(0, 1)) + Int32.Parse(vCalculo.ToString().Substring(1, 1));
            }

            if (vnTotal % 10 == 0)
                return true;
            else
                return false;
        }
    }
}