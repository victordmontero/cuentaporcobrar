﻿$(".fecha").datepicker({
    monthNames: [
        "Enero", "Febrero", "Marzo", "Abril",
        "Mayo", "Junio", "Julio", "Agosto",
        "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ],
    dayNamesShort: [
        "Lu", "Mar", "Mier", "Jue", "Vier", "Sab", "Dom"
    ],
    dayNamesMin: [
        "Lu", "Mar", "Mier", "Jue", "Vier", "Sab", "Dom"
    ],
    nextText: "Siguiente",
    prevText: "Anterior",
    closeText: "Cerrar"
});