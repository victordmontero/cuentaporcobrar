﻿using CuentaPorCobrar.DataAccess.Contexto;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CuentaPorCobrar.Views.Reporte
{
    public partial class Reporte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var contexto = new CXCContexto();
                int antiguedadSaldo = int.Parse(Session["antiguedadSaldo"].ToString());
                var resultados = contexto.Balances.ToList().Where(c => c.Corte.DayOfYear <= antiguedadSaldo + DateTime.Today.DayOfYear);

                ReportDataSource rds = new ReportDataSource();
                rds.Value = resultados;
                rds.Name = "AntiguedadSaldoDtSet";

                AntiguedadRpt.LocalReport.DataSources.Clear();
                AntiguedadRpt.LocalReport.DataSources.Add(rds);
                AntiguedadRpt.LocalReport.ReportEmbeddedResource = "Report1.rdlc";
                AntiguedadRpt.LocalReport.ReportPath = @"Reporte/Report1.rdlc";
                AntiguedadRpt.LocalReport.Refresh();
            }
        }
    }
}