﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CuentaPorCobrar.ViewModels
{
    public class CuentaViewModel
    {
        public int CuentaId { get; set; }
        
        [StringLength(40)]
        [Required(ErrorMessage = "El Campo Nombre debe tener una longitud máxima de 40")]
        public string Nombre { get; set; }
    }
}