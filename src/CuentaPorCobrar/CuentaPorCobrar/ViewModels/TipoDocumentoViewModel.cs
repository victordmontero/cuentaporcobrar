﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CuentaPorCobrar.ViewModels
{
    public class TipoDocumentoViewModel
    {
        public int TipoDocumentoId { get; set; }

        [Display(Name = "Descripción")]
        [StringLength(50)]
        [Required(ErrorMessage = "El Campo Descripción debe tener una longitud máxima de 50")]
        [RegularExpression(@"\w{40}", ErrorMessage = "Esta descripción es demasiada larga")]
        public string Descripcion { get; set; }

        [Display(Name = "Cuenta")]
        public int CuentaId { get; set; }

        public virtual CuentaViewModel Cuenta { get; set; }
    }
}