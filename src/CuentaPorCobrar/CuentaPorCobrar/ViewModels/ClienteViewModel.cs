﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace CuentaPorCobrar.ViewModels
{
    public class ClienteViewModel
    {
        public int ClienteId { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "El Campo Nombre debe tener una longitud máxima de 50")]
        [RegularExpression(@"[\w\s]+", ErrorMessage = "Sólo letras del alfabeto")]
        public string Nombre { get; set; }

        [Display(Name = "Cédula")]
        [StringLength(13)]
        [Required(ErrorMessage = "El Campo Cédula debe tener una longitud máxima de 11 sin contar guiones")]
        [RegularExpression(@"\d{11}", ErrorMessage = "Este campo sólo acepta números")]
        public string Cedula { get; set; }

        [Display(Name = "Límite de Crédito")]
        [Range(1, double.MaxValue, ErrorMessage = "Esta cantidad es demasiada grande")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public decimal LimiteCredito { get; set; }
    }
}