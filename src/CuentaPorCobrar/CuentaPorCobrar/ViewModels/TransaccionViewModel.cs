﻿using CuentaPorCobrar.DataAccess.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

namespace CuentaPorCobrar.ViewModels
{
    public class TransaccionViewModel
    {
        public int TransaccionId { get; set; }

        [Display(Name = "Tipo de Documento")]
        [Required]
        public int TipoDocumentoId { get; set; }

        [Display(Name = "Cliente")]
        [Required]
        public int ClienteId { get; set; }

        [Display(Name = "Tipo de Movimiento")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public TipoMovimiento TipoMovimiento { get; set; }

        [Display(Name = "Número de Documento")]
        public Guid NumeroDocumento { get; set; }

        public SqlDateTime Fecha { get; set; }

        [Range(0, double.MaxValue)]
        [Required(ErrorMessage = "Este campo es requerido")]
        public decimal Monto { get; set; }

        public virtual ClienteViewModel Cliente { get; set; }
        public virtual TipoDocumentoViewModel TipoDocumento { get; set; }

        public TransaccionViewModel()
        {
            Fecha = DateTime.Now;
            NumeroDocumento = Guid.NewGuid();
        }
    }
}