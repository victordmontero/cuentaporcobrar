﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CuentaPorCobrar.ViewModels
{
    public class BalanceViewModel
    {
        public int BalanceId { get; set; }

        [Display(Name = "Cliente")]
        public int ClienteId { get; set; }

        [Display(Name = "Fecha de Corte")]
        [Required(ErrorMessage="Este campo es requerido")]
        public DateTime Corte { get; set; }

        [Display(Name = "Saldo por Antiguedad Promedio")]
        [Range(1, byte.MaxValue)]
        [Required(ErrorMessage = "Este campo es requerido")]
        public int AntiguedadPromedioSaldos { get; set; }

        [Range(double.MinValue, double.MaxValue)]
        [Required(ErrorMessage = "Este campo es requerido")]
        public decimal Monto { get; set; }

        public virtual ClienteViewModel Cliente { get; set; }
    }
}