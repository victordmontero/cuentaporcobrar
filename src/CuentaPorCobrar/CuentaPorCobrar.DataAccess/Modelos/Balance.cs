﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaPorCobrar.DataAccess.Modelos
{
    public class Balance
    {
        public int BalanceId { get; set; }
        public int ClienteId { get; set; }
        public DateTime Corte { get; set; }
        public int AntiguedadPromedioSaldos { get; set; }
        public decimal Monto { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}
