﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaPorCobrar.DataAccess.Modelos
{
    public class Transaccion
    {
        public int TransaccionId { get; set; }
        public int TipoDocumentoId { get; set; }
        public int ClienteId { get; set; }
        public TipoMovimiento TipoMovimiento { get; set; }
        public Guid NumeroDocumento { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Monto { get; set; }

        public virtual Cliente Cliente { get; set; }
        public virtual TipoDocumento  TipoDocumento { get; set; }
    }
}
