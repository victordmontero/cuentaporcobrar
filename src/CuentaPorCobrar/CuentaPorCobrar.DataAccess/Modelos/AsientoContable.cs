﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaPorCobrar.DataAccess.Modelos
{
    public class AsientoContable
    {
        public int AsientoContableId { get; set; }
        public int ClienteId { get; set; }
        public string Descripcion { get; set; }
        public string Cuenta { get; set; }
        public TipoMovimiento TipoMovimiento { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Monto { get; set; }
        public bool Estado { get; set; }

        public AsientoContable()
        {
            Fecha = DateTime.Now;
        }
    }
}
