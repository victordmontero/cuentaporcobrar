﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaPorCobrar.DataAccess.Modelos
{
    public class Cuenta
    {
        public int CuentaId { get; set; }
        public string Nombre { get; set; }
    }
}
