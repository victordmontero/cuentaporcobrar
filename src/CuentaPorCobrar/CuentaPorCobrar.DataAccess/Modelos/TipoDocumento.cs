﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaPorCobrar.DataAccess.Modelos
{
    public class TipoDocumento
    {
        public int TipoDocumentoId { get; set; }
        public string Descripcion { get; set; }
        public int CuentaId { get; set; }
        public bool Estado { get; set; }

        public virtual Cuenta Cuenta { get; set; }

        public TipoDocumento()
        {
            Estado = true;
        }
    }
}
