﻿using CuentaPorCobrar.DataAccess.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaPorCobrar.DataAccess.Contexto
{
    public class CXCContexto : DbContext
    {
        /// <summary>
        /// Contexto Principal
        /// </summary>
        public CXCContexto()
            : base("CuentaXCobrar")
        {
            Database.SetInitializer<CXCContexto>(null);
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<TipoDocumento> TiposDocumento { get; set; }
        public DbSet<Balance> Balances { get; set; }
        public DbSet<Transaccion> Transacciones { get; set; }
        public DbSet<AsientoContable> AsientosContables { get; set; }
        public DbSet<Cuenta> Cuentas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            #region Mapeando Llaves Primarias
            modelBuilder.Entity<AsientoContable>().HasKey(c => c.AsientoContableId);
            modelBuilder.Entity<Balance>().HasKey(c => c.BalanceId);
            modelBuilder.Entity<Cliente>().HasKey(c => c.ClienteId);
            modelBuilder.Entity<TipoDocumento>().HasKey(c => c.TipoDocumentoId);
            modelBuilder.Entity<Transaccion>().HasKey(c => c.TransaccionId);
            modelBuilder.Entity<Cuenta>().HasKey(c => c.CuentaId);
            #endregion

            #region Nombre de Tablas
            modelBuilder.Entity<AsientoContable>().ToTable("AsientosContables");
            modelBuilder.Entity<Balance>().ToTable("Balances");
            modelBuilder.Entity<Cliente>().ToTable("Clientes");
            modelBuilder.Entity<TipoDocumento>().ToTable("TiposDocumento");
            modelBuilder.Entity<Transaccion>().ToTable("Transacciones");
            modelBuilder.Entity<Cuenta>().ToTable("Cuentas");
            #endregion

            #region Restricciones
            modelBuilder.Entity<Cliente>().Property(c => c.Nombre).HasMaxLength(50).IsUnicode(false).HasColumnType("VARCHAR");
            modelBuilder.Entity<Cliente>().Property(c => c.Cedula).HasMaxLength(11).IsUnicode(false).HasColumnType("CHAR");
            modelBuilder.Entity<TipoDocumento>().Property(c => c.Descripcion).HasMaxLength(50).IsUnicode(false).HasColumnType("VARCHAR");
            modelBuilder.Entity<Cuenta>().Property(c => c.Nombre).HasMaxLength(40).IsUnicode(false).HasColumnType("VARCHAR");
            modelBuilder.Entity<Cliente>().Property(c => c.Cedula).HasColumnAnnotation(
                "Index",
                new IndexAnnotation(new[]{
                    new IndexAttribute("UQ_Cedula"){ IsUnique = true }
                }));
            #endregion

            base.OnModelCreating(modelBuilder);
        }
    }
}
