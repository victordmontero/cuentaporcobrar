﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CuentaPorCobrar.Service.Modelos
{
    public enum TipoMovimiento : byte
    {
        Credito,
        Debito
    }
}
