﻿using CuentaPorCobrar.Service.Modelos;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;

namespace CuentaPorCobrar.Service
{
    /// <summary>
    /// Summary description for CuentaPorCobrar
    /// </summary>
    [WebService(Namespace = "http://unapec.edu.do/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CuentaPorCobrar : System.Web.Services.WebService
    {

        [WebMethod]
        public bool HacerTransaccion(Transaccion transaccion)
        {
            int registroAfectados = 0;
            string connString = WebConfigurationManager.ConnectionStrings["CuentaPorCobrar"].ToString();
            string consulta = string.Format("INSERT INTO [dbo].[Transacciones] VALUES({0},{1},{2},'{3}','{4}',{5});", transaccion.TipoDocumentoId,
                transaccion.ClienteId,
                (byte)transaccion.TipoMovimiento,
                transaccion.NumeroDocumento,
                transaccion.Fecha,
                transaccion.Monto
                );
            var conn = new SqlConnection(connString);

            conn.Open();
            using (var sqlCommand = new SqlCommand(consulta, conn))
            {
                registroAfectados = sqlCommand.ExecuteNonQuery();
            }
            conn.Close();
            return registroAfectados > 0;
        }
    }
}
