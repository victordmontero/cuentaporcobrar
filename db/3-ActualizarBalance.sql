﻿GO
CREATE TRIGGER ActualizarBalance
ON Transacciones
AFTER INSERT, UPDATE
AS
BEGIN
	IF (@@ROWCOUNT = 0) RETURN;
	SET NOCOUNT ON;
	DECLARE @CREDITO INT = 0,
			@DEBITO INT = 1,
			@ClienteId INT,
			@TipoDocumento INT,
			@Monto DECIMAL(18,2)

	SELECT @ClienteId = ClienteId,
	@TipoDocumento = TipoMovimiento,
	@Monto = Monto
	FROM inserted

	IF (@TipoDocumento=@CREDITO)
		UPDATE Balances
		SET Monto = Monto-@Monto
		WHERE ClienteId = @ClienteId
	ELSE
		UPDATE Balances
		SET Monto = Monto+@Monto
		WHERE ClienteId = @ClienteId

	RETURN;
END

GO



CREATE PROCEDURE ReporteAntiguedadSaldo
AS
BEGIN
	SELECT C.Nombre,
		C.Cedula,
		B.Corte,
		B.AntiguedadPromedioSaldos,
		B.Monto
	FROM Clientes C
	JOIN Balances B
	ON C.ClienteId = B.ClienteId
END